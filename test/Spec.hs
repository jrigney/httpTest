{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Lib (app1)
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant
import Test.Hspec
import Test.Hspec.Wai
import Test.Hspec.Wai.JSON

main :: IO ()
main = hspec spec

app :: IO (Application)
app = do
  manager <- newManager defaultManagerSettings
  return $ app1 manager

spec :: Spec
spec =
  with app $ do
    describe "GET /" $ do
      it "responds with 200" $ do get "/hello" `shouldRespondWith` 200
      it "responds with 'Simple'" $ do
        get "/hello" `shouldRespondWith`
          200
          {matchHeaders = ["Content-Type" <:> "application/json;charset=utf-8"]}
      it "responds with 'coward'" $ do
        get "/hello" `shouldRespondWith`
          [json|{msg: "Hello, anonymous coward"}|]
          {matchHeaders = ["Content-Type" <:> "application/json;charset=utf-8"]}
