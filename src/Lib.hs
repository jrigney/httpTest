{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( app1
    ) where

import Data.Time.Calendar
import Servant
import GHC.Generics
import Data.Aeson.Types
import Data.List
import Control.Monad.IO.Class (liftIO)
import System.Directory (doesFileExist) --directory package
import JsonTestClient (ipAddressRequest, addr)
import Network.HTTP.Client (Manager)
import Debug.Trace (trace)
import Control.Lens

type API = "position" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Position
      :<|> "hello" :> QueryParam "name" String :> Get '[JSON] HelloMessage
      :<|> "marketing" :> ReqBody '[JSON] ClientInfo :> Post '[JSON] Email
      :<|> "myfile.txt" :> Get '[JSON] FileContent
      :<|> "nofile" :> Get '[JSON] FileContent

server3 :: Server API
server3 = position
     :<|> hello
     :<|> marketing
     :<|> file
     :<|> noFile

  where position :: Int -> Int -> Handler Position
        position x y = return (Position x y)

        hello :: Maybe String -> Handler HelloMessage
        hello mname = return . HelloMessage $ case mname of
          Nothing -> "Hello, anonymous coward"
          Just n  -> "Hello, " ++ n

        marketing :: ClientInfo -> Handler Email
        marketing clientinfo = return (emailForClient clientinfo)

        file :: Handler FileContent
        file = do
          filecontent <- liftIO(readFile "myfile.txt")
          return (FileContent filecontent)

        noFile :: Handler FileContent
        noFile = do
          exists <- liftIO(doesFileExist "noFileHere")
          if exists
            then liftIO(readFile "noFileHere") >>= return . FileContent
            else throwError custom404Err
          where custom404Err = err404{errBody = "noFileHere is not here"}

type JsonTestApi = "jsontest" :> Get '[JSON] IpAddress

jsontestServer :: Manager -> Server JsonTestApi
--jsontestServer a | trace("#### jsontestServer") False = undefined
jsontestServer mgr = ipaddr
  where ipaddr :: Handler IpAddress
        ipaddr = do
          resp <- liftIO(ipAddressRequest mgr)
          case resp of
           Left anError -> throwError err504{errBody = "jsontest error"} --TODO fix this error
           Right val -> return $ IpAddress $ view addr val

data IpAddress = IpAddress
    {ipAddress :: String} deriving(Show, Generic)

instance ToJSON IpAddress

type InternalApi = "version" :> Get '[JSON] InternalVersion
                  :<|> "system_health" :> Get '[JSON] Imok


internalServer :: Server InternalApi
internalServer = version :<|> health
  where version :: Handler InternalVersion
        version = return $ InternalVersion "two"

        health :: Handler Imok
        health = return $ Imok "imok"

type CombinedApi = "internal" :> InternalApi
                :<|> API
                :<|> JsonTestApi

combinedServers :: Manager -> Server CombinedApi
--combinedServers a | trace("#### combinedServers") False = undefined
combinedServers mgr = internalServer :<|> server3 :<|> jsontestServer mgr

apis :: Proxy CombinedApi
apis = Proxy

app1 :: Manager -> Application
app1 mgr = serve apis $ combinedServers mgr

--- API for values of type 'a'
-- indexed by values of type 'i'
type APIFor a i =
       Get '[JSON] [a] -- list 'a's
  :<|> ReqBody '[JSON] a :> PostNoContent '[JSON] NoContent -- add an 'a'
  :<|> Capture "id" i :>
         ( Get '[JSON] a -- view an 'a' given its "identifier" of type 'i'
      :<|> ReqBody '[JSON] a :> PutNoContent '[JSON] NoContent -- update an 'a'
      :<|> DeleteNoContent '[JSON] NoContent -- delete an 'a'
         )

-- Build the appropriate 'Server'
-- given the handlers of the right type.
serverFor :: Handler [a] -- handler for listing of 'a's
          -> (a -> Handler NoContent) -- handler for adding an 'a'
          -> (i -> Handler a) -- handler for viewing an 'a' given its identifier of type 'i'
          -> (i -> a -> Handler NoContent) -- updating an 'a' with given id
          -> (i -> Handler NoContent) -- deleting an 'a' given its id
          -> Server (APIFor a i)
serverFor handler add view update delete =
    ha
    :<|> ad
    :<|> id
    where ha = handler
          ad = add
          id i =
            vi i :<|> up i :<|> de i
            where
              vi = view
              up = update
              de = delete

-- implementation left as an exercise. contact us on IRC
-- or the mailing list if you get stuck!




--userAPI :: Proxy API
--userAPI = Proxy
--
---- 'serve' comes from servant and hands you a WAI Application,
---- which you can think of as an "abstract" web application,
---- not yet a webserver.
--app1 :: Application
--app1 = serve userAPI server3

data InternalVersion = InternalVersion
                     {version :: String}
                     deriving Generic

instance ToJSON InternalVersion

data Imok = Imok
          { status ::  String }
          deriving (Generic, Show)

instance ToJSON Imok

data Position = Position
  { xCoord :: Int
  , yCoord :: Int
  } deriving Generic

instance ToJSON Position

newtype HelloMessage = HelloMessage { msg :: String }
  deriving Generic

instance ToJSON HelloMessage

data ClientInfo = ClientInfo
  { clientName :: String
  , clientEmail :: String
  , clientAge :: Int
  , clientInterestedIn :: [String]
  } deriving Generic

instance FromJSON ClientInfo
instance ToJSON ClientInfo

data Email = Email
  { from :: String
  , to :: String
  , subject :: String
  , body :: String
  } deriving Generic

instance ToJSON Email

newtype FileContent = FileContent
                      {content :: String}
                      deriving Generic

instance ToJSON FileContent

failingHandler :: Handler()
failingHandler = throwError myerr
  where myerr :: ServantErr
        myerr = err503{errBody = "Sorry dear user."}

emailForClient :: ClientInfo -> Email
emailForClient c = Email from' to' subject' body'

  where from'    = "great@company.com"
        to'      = clientEmail c
        subject' = "Hey " ++ clientName c ++ ", we miss you!"
        body'    = "Hi " ++ clientName c ++ ",\n\n"
                ++ "Since you've recently turned " ++ show (clientAge c)
                ++ ", have you checked out our latest "
                ++ intercalate ", " (clientInterestedIn c)
                ++ " products? Give us a visit!"




