{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}

module Measurement() where

import Control.Monad.Trans.State.Strict(StateT(..))
import Data.Text
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.State (MonadState)
import Data.HashMap.Strict (HashMap)
import Prometheus (Counter, Histogram, buildRegistry, Registry, Metric,
                  MetricName, MetricHelp,
                  counter, register, publishRegistryMiddleware)

data Metrics = Metrics
  { iterations :: Counter
  }

registerMetric
  :: (MonadState Registry m, MonadIO m)
  => MetricName
     -- ^ The name of this metric.
  -> MetricHelp
     -- ^ Descriptive text about what this metric measures.
  -> HashMap Text Text
     -- ^ A map of /static/ labels, that will be applied whenever the metric is sampled.
     -- For dynamic labels (labels that change), see 'addLabel'.
  -> Metric a
     -- ^ The metric to register.
  -> m a
registerMetric name help labels metric =
  (register name help labels metric)
