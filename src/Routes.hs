{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Routes
  ( app
  ) where

import Control.Lens
import Control.Monad.IO.Class (liftIO)
import Data.Aeson.Types
import Data.List
import Data.Time.Calendar
import Debug.Trace (trace)
import GHC.Generics
import JsonTestClient (addr, ipAddressRequest)
import Network.HTTP.Client (Manager)
import Servant
import System.Directory (doesFileExist) --directory package

type CombinedApi = "internal" :> InternalApi :<|> JsonTestApi

type JsonTestApi = "jsontest" :> Get '[ JSON] IpAddress

type InternalApi
   = "version" :> Get '[ JSON] InternalVersion :<|> "system_health" :> Get '[ JSON] Imok

--'server' comes from servant
app :: Manager -> Application
app mgr = serve apis $ combinedServers mgr

apis :: Proxy CombinedApi
apis = Proxy

combinedServers :: Manager -> Server CombinedApi
combinedServers mgr = internalServer :<|> jsontestServer mgr

jsontestServer :: Manager -> Server JsonTestApi
jsontestServer mgr = ipaddr
  where
    ipaddr :: Handler IpAddress
    ipaddr = do
      resp <- liftIO (ipAddressRequest mgr)
      case resp of
        Left anError -> throwError err504 {errBody = "jsontest error"} --TODO fix this error
        Right val -> return $ IpAddress $ view addr val

data IpAddress = IpAddress
  { ipAddress :: String
  } deriving (Show, Generic)

instance ToJSON IpAddress

internalServer :: Server InternalApi
internalServer = version :<|> health
  where
    version :: Handler InternalVersion
    version = return $ InternalVersion "two"
    health :: Handler Imok
    health = return $ Imok "imok"

data InternalVersion = InternalVersion
  { version :: String
  } deriving (Generic)

instance ToJSON InternalVersion

data Imok = Imok
  { status :: String
  } deriving (Generic, Show)

instance ToJSON Imok
