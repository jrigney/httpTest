{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module JsonTestClient
    ( ipAddressRequest
    , addr
    , IpAddress(IpAddress)
    ) where

import Data.Aeson
import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.Client
import Network.HTTP.Client (Manager)
import Debug.Trace (trace)
import Control.Lens



addr :: Lens' IpAddress String
addr = lens ip (\ipaddr v -> ipaddr{ip = v})

type API = Get '[JSON] IpAddress

api :: Proxy API
api = Proxy

ipAddressApi :: ClientM IpAddress
ipAddressApi = client api


jsonTestHost :: BaseUrl
jsonTestHost = BaseUrl Http "ip.jsontest.com" 80 ""

ipAddressRequest :: Manager -> IO (Either ServantError IpAddress)
ipAddressRequest mgr = runClientM ipAddress (ClientEnv mgr jsonTestHost)

ipAddress :: ClientM (IpAddress)
ipAddress = do
  ip <- ipAddressApi
  return (ip)

data IpAddress = IpAddress
  { ip :: String
  } deriving (Show, Generic)

instance FromJSON IpAddress

