{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Prometheus (Counter, Histogram, buildRegistry, 
                  counter, register, publishRegistryMiddleware)
import Routes
import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Client (newManager, defaultManagerSettings)

data Metrics = Metrics
  { iterations :: Counter
  }

main :: IO ()
main = do
  (metrics, registry) <- buildRegistry $ do
    iterations <- register "iterations" "Total" mempty counter
    return Metrics{..}

  manager <- newManager defaultManagerSettings
  run 28558 $ (publishRegistryMiddleware ["metrics"] registry . app) manager
